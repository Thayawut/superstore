from django.db import models

# Create your models here.
class Superstore(models.Model):
    order_id = models.CharField(max_length=50)
    order_date = models.DateField()
    ship_date = models.DateField()
    ship_mode = models.CharField(max_length=50)
    customer_id = models.CharField(max_length=50)
    customer_name = models.CharField(max_length=50)
    segment = models.CharField(max_length=50)
    country = models.CharField(max_length=50)
    city = models.CharField(max_length=50)
    state = models.CharField(max_length=50)
    postal_code = models.IntegerField()
    region	= models.CharField(max_length=50)
    product_id = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    sub_category  = models.CharField(max_length=50)
    product_name  = models.CharField(max_length=150)
    sales  = models.FloatField()
    quantity  = models.IntegerField()
    discount  = models.FloatField()
    profit = models.FloatField()
    

