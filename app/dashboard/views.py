from django.core.checks import messages
from django.db.models.aggregates import Min
from django.db.models.expressions import Value
from django.shortcuts import render
from .models import Superstore
from django.db.models import Sum, Count, FloatField,Max, Func
from django.db import models
from django.db.models.functions import ExtractYear, ExtractMonth
import pandas as pd

class Year(Func):
    function = 'EXTRACT'
    template = '%(function)s(YEAR from %(expressions)s)'
    output_field = models.IntegerField()

def home(request):
    return render(request, 'index.html')

def dashboard(request):
    
    year = request.GET.get('year')

    sale = Superstore.objects
    profit = Superstore.objects
    order = Superstore.objects
    profitmargin = Superstore.objects
    furnituresales = Superstore.objects
    Officesales = Superstore.objects
    technologysales = Superstore.objects
    furnitureprofit = Superstore.objects
    technologyprofit = Superstore.objects
    Officeprofit = Superstore.objects
    years = Superstore.objects.annotate(year=ExtractYear('ship_date')).values('year').annotate(cyear=Count('year')).values('year')

    if (year):
        message = 'Sale Report Jan 1, '+year+' - Dec 31, '+year
        sale = sale.filter(ship_date__year=year)
        profit = profit.filter(ship_date__year=year)
        order = order.filter(ship_date__year=year)
        profitmargin = profitmargin.filter(ship_date__year=year)
        furnituresales = furnituresales.filter(category='Furniture',ship_date__year=year)
        Officesales = Officesales.filter(category='Office Supplies',ship_date__year=year)
        technologysales = technologysales.filter(category='Technology',ship_date__year=year)
        furnitureprofit =furnitureprofit.filter(category='Furniture',ship_date__year=year)
        technologyprofit = technologyprofit.filter(category='Technology',ship_date__year=year)
        Officeprofit = Officeprofit.filter(category='Office Supplies',ship_date__year=year)
        sale_summary = list()
        for m in range(12):
            totalSale = sale.filter(ship_date__month=(m+1)).annotate(month=ExtractMonth('ship_date')).values('month').annotate(cmonth=Count('month')).aggregate(Sum('sales'))
            if totalSale['sales__sum'] is not None:
                sale_summary.append(totalSale['sales__sum'])
            else:
                sale_summary.append(0)
        graph_labels = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Otc', 'Nov', 'Dec']
        graph_values = sale_summary
    else:
        start_year = years.order_by('year').first()
        end_year = years.order_by('year').last()
        message = 'Sale Report Jan 1, '+str(start_year['year'])+' - Dec 31, '+str(end_year['year'])
        sale_summary = sale.annotate(year=ExtractYear('ship_date')).values('year').annotate(totlaSale=Sum('sales')).order_by('year')
        graph_labels = sale_summary.values_list('year', flat=True)
        graph_values = sale_summary.values_list('totlaSale', flat=True)
    
    sale = sale.aggregate(Sum('sales'))
    profit = profit.aggregate(Sum('profit'))
    order = order.values('order_id').distinct().count()
    profitmargin = profitmargin.aggregate(price_diff=(Sum('profit') / Sum('sales'))*100)
    furnituresales = furnituresales.aggregate(Sum('sales'))
    Officesales = Officesales.aggregate(Sum('sales'))
    technologysales = technologysales.aggregate(Sum('sales'))
    furnitureprofit = furnitureprofit.aggregate(Sum('profit'))
    technologyprofit = technologyprofit.aggregate(Sum('profit'))
    Officeprofit = Officeprofit.aggregate(Sum('profit'))

    filter_years = years.order_by('-year').values_list('year', flat=True)
    context = {
        'message':message,
        'sale':sale,
        'profit':profit,
        'order':order,
        'profitmargin':profitmargin,
        'furnituresales':furnituresales,
        'Officesales':Officesales,
        'technologysales':technologysales,
        'furnitureprofit':furnitureprofit,
        'Officeprofit':Officeprofit,
        'technologyprofit':technologyprofit,
        'filter_years':list(filter_years),
        'graph_labels':list(graph_labels),
        'graph_values':list(graph_values),
        'year':year
        }
    return render(request, "dashboard.html" ,context)

