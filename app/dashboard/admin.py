from django.contrib import admin

# Register your models here.
from .models import Superstore

admin.site.register(Superstore)