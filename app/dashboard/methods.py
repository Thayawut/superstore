import pandas as pd
from .models import Superstore
from datetime import datetime

# dataset from https://www.kaggle.com/aungpyaeap/supermarket-sales
# headers changed and invoice number col removed
def csv_to_db():
    df = pd.read_csv('superstore.csv') # use pandas to read the csv
    records = df.to_records()  # convert to records

    # loop through and create a purchase object using django
    for record in records:
        superstore = Superstore(
            order_id=record[2],
            order_date=datetime.strptime(record[3], '%m/%d/%Y').date(),
            ship_date=datetime.strptime(record[4], '%m/%d/%Y').date(),
            ship_mode=record[5],
            customer_id=record[6],
            customer_name=record[7],
            segment=record[8],
            country=record[9],
            city=record[10],
            state=record[11],
            postal_code=record[12],
            region=record[13],
            product_id=record[14],
            category=record[15],
            sub_category=record[16],
            product_name=record[17],
            sales=record[18],
            quantity=record[19],
            discount=record[20],
            profit=record[21],
        )
        superstore.save()
